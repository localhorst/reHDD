/**
 * @file    tui.h
 * @brief   display user interface
 * @author  hendrik schutter
 * @date    03.08.2020
 */

#ifndef TUI_H_
#define TUI_H_

#include "reHDD.h"

#define COLOR_AREA_STDSCR 1
#define COLOR_AREA_OVERVIEW 2
#define COLOR_AREA_ENTRY_EVEN 3
#define COLOR_AREA_ENTRY_ODD 4
#define COLOR_AREA_ENTRY_SELECTED 5
#define COLOR_AREA_DETAIL 6

class TUI
{
protected:
public:
    enum UserInput
    {
        UpKey,
        DownKey,
        Abort,
        Shred,
        ShredAll,
        Delete,
        Enter,
        ESC,
        Undefined
    };
    struct MenuState
    {
        bool bAbort;
        bool bShred;
        bool bDelete;
        bool bConfirmShred;
        bool bConfirmDelete;
    };

    TUI(void);

    static void initTUI();

    void updateTUI(list<Drive> *plistDrives, uint8_t u8SelectedEntry);

    static enum UserInput readUserInput();

private:
    static string sCpuUsage;
    static string sRamUsage;
    static string sLocalTime;

    WINDOW *overview;
    WINDOW *systemview;
    WINDOW *detailview;
    WINDOW *menuview;
    WINDOW *dialog;
    WINDOW *smartWarning;

    static void centerTitle(WINDOW *pwin, const char *title);
    static WINDOW *createOverViewWindow(int iXSize, int iYSize);
    static WINDOW *createDetailViewWindow(int iXSize, int iYSize, int iXStart, Drive drive);
    static WINDOW *overwriteDetailViewWindow(int iXSize, int iYSize, int iXStart);
    static WINDOW *createEntryWindow(int iXSize, int iYSize, int iXStart, int iYStart, int iListIndex, string sModelFamily, string sSerial, string sCapacity, string sState, string sTime, string sSpeed, string sTemp, bool bSelected);
    static WINDOW *createSystemStats(int iXSize, int iYSize, int iXStart, int iYStart);
    static WINDOW *createMenuView(int iXSize, int iYSize, int iXStart, int iYStart, struct MenuState menustate);
    static WINDOW *createDialog(int iXSize, int iYSize, int iXStart, int iYStart, string selectedTask, string optionA, string optionB);
    static WINDOW *createFrozenWarning(int iXSize, int iYSize, int iXStart, int iYStart, string sPath, string sModelFamily, string sModelName, string sSerial, string sProgress);
    static WINDOW *createSmartWarning(int iXSize, int iYSize, int iXStart, int iYStart, string sPath, uint32_t u32PowerOnHours, uint32_t u32PowerCycles, uint32_t u32ErrorCount, uint32_t u32Temperature);
    static WINDOW *createZeroChecksumWarning(int iXSize, int iYSize, int iXStart, int iYStart, string sPath, string sModelFamily, string sModelName, string sSerial, uint32_t u32Checksum);

    void displaySelectedDrive(Drive drive, int stdscrX, int stdscrY);
    string formatTimeDuration(time_t u32Duration);
    string formatSpeed(time_t u32ShredTimeDelta, unsigned long ulWrittenBytes);
    static void vTruncateText(string *psText, uint16_t u16MaxLenght);
};
#endif // TUI_H_