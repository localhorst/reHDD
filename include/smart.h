/**
 * @file    smart.h
 * @brief   read S.M.A.R.T values
 * @author  hendrik schutter
 * @date    01.05.2020
 */

#ifndef SMART_H_
#define SMART_H_

#include "reHDD.h"

class SMART
{
protected:
public:
    static void readSMARTData(Drive *drive);

private:
    SMART(void);

    static bool parseExitStatus(string sLine, uint8_t &status);
    static bool parseModelFamily(string sLine, string &modelFamily);
    static bool parseModelName(string sLine, string &modelName);
    static bool parseSerial(string sLine, string &serial);
    static bool parseCapacity(string sLine, uint64_t &capacity);
    static bool parseErrorCount(string sLine, uint32_t &errorCount);
    static bool parsePowerOnHours(string sLine, uint32_t &powerOnHours);
    static bool parsePowerCycles(string sLine, uint32_t &powerCycles);
    static bool parseTemperature(string sLine, uint32_t &temperature);
};

#endif // SMART_H_