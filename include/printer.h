/**
 * @file    printer.h
 * @brief   Send drive data to printer service using ipc msg queue
 * @author  Hendrik Schutter
 * @date    24.11.2022
 */

#ifndef PRINTER_H_
#define PRINTER_H_

#include "reHDD.h"

#include <sys/ipc.h>
#include <sys/msg.h>

#define STR_BUFFER_SIZE 64U
#define IPC_MSG_QUEUE_KEY 0x1B11193C0

typedef struct
{
    char caDriveIndex[STR_BUFFER_SIZE];
    char caDriveHours[STR_BUFFER_SIZE];
    char caDriveCycles[STR_BUFFER_SIZE];
    char caDriveErrors[STR_BUFFER_SIZE];
    char caDriveShredTimestamp[STR_BUFFER_SIZE];
    char caDriveShredDuration[STR_BUFFER_SIZE];
    char caDriveCapacity[STR_BUFFER_SIZE];
    char caDriveState[STR_BUFFER_SIZE];
    char caDriveModelFamily[STR_BUFFER_SIZE];
    char caDriveModelName[STR_BUFFER_SIZE];
    char caDriveSerialnumber[STR_BUFFER_SIZE];
    char caDriveReHddVersion[STR_BUFFER_SIZE];

} t_driveData;

typedef struct
{
    long msg_queue_type;
    t_driveData driveData;
} t_msgQueueData;

class Printer
{
protected:
public:
    static Printer *getPrinter();
    void print(Drive *drive);

private:
    static bool instanceFlag;
    static Printer *single;
    int msqid;
    Printer();
    ~Printer();
};
#endif // PRINTER_H_