/**
 * @file    reHDD.h
 * @brief   app logic header
 * @author  hendrik schutter
 * @date    01.05.2020
 */

#ifndef REHDD_H_
#define REHDD_H_

#define REHDD_VERSION "V1.1.3"

// Drive handling Settings
#define WORSE_HOURS 19200    // mark drive if at this limit or beyond
#define WORSE_POWERUP 10000  // mark drive if at this limit or beyond
#define WORSE_TEMPERATURE 55 // mark drive if at this limit or beyond
#define SHRED_ITERATIONS 3U
#define FROZEN_TIMEOUT 20                           // After this timeout (minutes) the drive will be marked as frozen, if no progress
#define METRIC_THRESHOLD 3L * 1000L * 1000L * 1000L // calc shred speed with this minimum of time delta

// Logger Settings
#define LOG_PATH "./reHDD.log"
#define DESCRIPTION "reHDD - Copyright Hendrik Schutter 2024"
#define DEVICE_ID "generic"
#define SOFTWARE_VERSION REHDD_VERSION
#define HARDWARE_VERSION "generic"

// #define LOG_LEVEL_HIGH  //log everything, like drive scan thread
#ifndef LOG_LEVEL_HIGH
#define LOG_LEVEL_LOW // log only user actions and tasks
#endif

// Logic
//#define DRYRUN       // don't touch the drives
#define FROZEN_ALERT // show alert if drive is frozen
#define ZERO_CHECK   // check drive after shred if all bytes are zero, show alert if this fails

// IPC pipes
#define READ 0
#define WRITE 1

#include <iostream>
#include <string>
#include <fstream>
#include <tuple>
#include <list>
#include <time.h>
#include <chrono>
#include <curses.h>
#include <thread>
#include <unistd.h>
#include <mutex>
#include <sys/select.h>
#include <algorithm>
#include <cstring>
#include <sstream>
#include <iomanip>
#include <signal.h>

using namespace std;

#include "drive.h"
#include "smart.h"
#include "shred.h"
#include "delete.h"
#include "tui.h"
#include "printer.h"
#include "logger/logger.h"

extern Logger *logging;

template <typename T, typename I>
T *iterator_to_pointer(I i)
{
    return (&(*i));
}

class reHDD
{
protected:
public:
    reHDD(void);
    static void app_logic();

private:
    static void searchDrives(list<Drive> *plistDrives);
    static void printDrives(list<Drive> *plistDrives);
    static void startShredAllDrives(list<Drive> *plistDrives);
    static void updateShredMetrics(list<Drive> *plistDrives);
    static void filterIgnoredDrives(list<Drive> *plistDrives);
    static void filterInvalidDrives(list<Drive> *plistDrives);
    static void filterNewDrives(list<Drive> *plistOldDrives, list<Drive> *plistNewDrives);
    static void addSMARTData(list<Drive> *plistDrives);
    static void ThreadScanDevices();
    static void ThreadUserInput();
    static void ThreadShred(Drive *const pDrive);
    static void ThreadDelete();
    static void ThreadCheckFrozenDrives();
    static void handleArrowKey(TUI::UserInput userInput);
    static void handleEnter();
    static void handleESC();
    static void handleAbort();
    static Drive *getSelectedDrive();
    static bool getSystemDrive(string &systemDrive);
};

#endif // REHDD_H_
