/**
 * @file    delete.h
 * @brief   delete drive
 * @author  hendrik schutter
 * @date    23.08.2020
 */

#ifndef DELETE_H_
#define DELETE_H_

#include "reHDD.h"

class Delete
{
protected:
public:
    static void deleteDrive(Drive *drive);

private:
    Delete(void);
};

#endif // DELETE_H_
