/**
 * @file    shred.h
 * @brief   shred drive
 * @author  hendrik schutter
 * @date    03.05.2020
 */

#ifndef SHRED_H_
#define SHRED_H_

#include "reHDD.h"

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define CHUNK_SIZE 1024 * 1024 * 32 // amount of bytes that are overwritten at once --> 32MB
#define TFNG_DATA_SIZE CHUNK_SIZE   // amount of bytes used by tfng

// #define DEMO_DRIVE_SIZE 1024*1024*256L // 256MB
// #define DEMO_DRIVE_SIZE 1024*1024*1024L // 1GB
// #define DEMO_DRIVE_SIZE 5*1024*1024*1024L // 5GB
// #define DEMO_DRIVE_SIZE 1024*1024*1024*10L // 10GB

typedef int fileDescriptor;

class Shred
{
protected:
public:
    Shred();
    ~Shred();
    int shredDrive(Drive *drive, int *ipSignalFd);

private:
    fileDescriptor randomSrcFileDiscr;
    fileDescriptor driveFileDiscr;
    unsigned char caTfngData[TFNG_DATA_SIZE];
    unsigned char caReadBuffer[CHUNK_SIZE];
    unsigned long ulDriveByteSize;
    unsigned long ulDriveByteOverallCount = 0; // all bytes shredded in all iterations + checking -> used for progress calculation
    double d32Percent = 0.0;
    double d32TmpPercent = 0.0;

    inline double calcProgress();
    int iRewindDrive(fileDescriptor file);
    unsigned long getDriveSizeInBytes(fileDescriptor file);
    unsigned int uiCalcChecksum(fileDescriptor file, Drive *drive, int *ipSignalFd);
    void cleanup();
};

#endif // SHRED_H_
