/**
 * @file    printer.cpp
 * @brief   Send drive data to printer service using ipc msg queue
 * @author  Hendrik Schutter
 * @date    24.11.2022
 */

#include "../include/reHDD.h"

bool Printer::instanceFlag = false;
Printer *Printer::single = NULL;

/**
 * \brief   create new Printer instance
 * \param	path to log file
 * \param	struct with data
 * \return  instance of Printer
 */
Printer::Printer()
{
    if (-1 == (this->msqid = msgget((key_t)IPC_MSG_QUEUE_KEY, IPC_CREAT | 0666)))
    {
        Logger::logThis()->error("Printer: Create mgs queue failed!");
    }
}

/**
 * \brief   deconstructor
 * \return  void
 */
Printer::~Printer()
{
    instanceFlag = false;
}

/**
 * \brief   send data to msg queue
 * \return  void
 */
void Printer::print(Drive *drive)
{
    t_msgQueueData msgQueueData;
    msgQueueData.msg_queue_type = 1;

    sprintf(msgQueueData.driveData.caDriveIndex, "%i", 42); // TODO: get from tui
    sprintf(msgQueueData.driveData.caDriveState, "shredded");
    strcpy(msgQueueData.driveData.caDriveModelFamily, drive->getModelFamily().c_str());
    strcpy(msgQueueData.driveData.caDriveModelName, drive->getModelName().c_str());
    sprintf(msgQueueData.driveData.caDriveCapacity, "%li", drive->getCapacity());
    strcpy(msgQueueData.driveData.caDriveSerialnumber, drive->getSerial().c_str());
    sprintf(msgQueueData.driveData.caDriveHours, "%i", drive->getPowerOnHours());
    sprintf(msgQueueData.driveData.caDriveCycles, "%i", drive->getPowerCycles());
    sprintf(msgQueueData.driveData.caDriveErrors, "%i", drive->getErrorCount());
    sprintf(msgQueueData.driveData.caDriveShredTimestamp, "%li", drive->getActionStartTimestamp());
    sprintf(msgQueueData.driveData.caDriveShredDuration, "%li", drive->getTaskDuration());
    sprintf(msgQueueData.driveData.caDriveReHddVersion, REHDD_VERSION);

    if (-1 == msgsnd(this->msqid, &msgQueueData, sizeof(t_msgQueueData) - sizeof(long), 0))
    {
        Logger::logThis()->error("Printer: Send mgs queue failed!");
    }
    else
    {
        Logger::logThis()->info("Printer: print triggered - Drive: " + drive->getSerial());
    }
}

/**
 * \brief   return a instance of the printer
 * \return  printer obj
 */
Printer *Printer::getPrinter()
{
    if (!instanceFlag)
    {
        single = new Printer(); // create new obj
        instanceFlag = true;
        return single;
    }
    else
    {
        return single; // return existing obj
    }
}