/**
 * @file    delete.cpp
 * @brief   delete drive
 * @author  hendrik schutter
 * @date    23.08.2020
 */

#include "../include/reHDD.h"

/**
 * \brief   delete drive with wipefs
 * \param	pointer of Drive instance
 * \return  void
 */
void Delete::deleteDrive(Drive *drive)

{
    size_t len = 0;     // lenght of found line
    char *cLine = NULL; // found line

#ifndef DRYRUN
    string sCMD = ("wipefs -af ");
    sCMD.append(drive->getPath());
    sCMD.append("*");
#endif

#ifdef DRYRUN
    // cout << "dryrun for " << drive->getPath() << endl;
    string sCMD = ("echo");
#endif

    const char *cpComand = sCMD.c_str();
    // cout << "delete: " << cpComand << endl;

    if (drive->bWasShredStarted == false)
    {
        //only start delete if the drive was not shredded before
        FILE *deleteCmdOutput = popen(cpComand, "r");

        while ((getline(&cLine, &len, deleteCmdOutput)) != -1)
        {
            // wipefs running
        }
        pclose(deleteCmdOutput);
    }
}
