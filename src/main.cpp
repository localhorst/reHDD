/**
 * @file    main.cpp
 * @brief   app entry point
 * @author  hendrik schutter
 * @date    01.05.2020
 */

#include "../include/reHDD.h"

/**
 * \brief   app entry point
 * \param	void
 * \return  Status-Code
 */
int main(void)
{
    // cout << "refurbishingHddTool" << endl;

    reHDD *app = new reHDD();
    app->app_logic();
    return EXIT_SUCCESS;
}