# reHDD

## Features:
* show S.M.A.R.T values of attached drives
* checking used drives for their next live based on threshold limits
* delete a drive instant with wipefs
* deleting a drive securely via overwriting
* only needs a display and keyboard
* process multiple drives at once

## Download USB Image ##
See reHDD-Bootable how the live image created: https://git.mosad.xyz/localhorst/reHDD-Bootable

Use [Etcher](https://www.balena.io/etcher/#download) or `dd` to create an bootable USB drive .

## Screenshot 
![Screenshot of reHDD with multiple drives in different states](https://git.mosad.xyz/localhorst/reHDD/raw/commit/c40dfe2cbb8f86490b49caf82db70a10015f06f9/doc/screenshot.png "Screenshot")

## Debian Build Notes

* `apt-get install ncurses-dev git make g++`
* `git submodule init`
* `git submodule update`
* `make release`

## Enable Label Printer ##

Just install [reHDDPrinter](https://git.mosad.xyz/localhorst/reHDDPrinter).
No further settings needed.

## Create Standalone with Debian 11

Instructions how to create a standalone machine that boots directly to reHDD. This is aimed for production use, like several drives a day shredding.
* Start reHDD after boot without login (as a tty1 shell)
* Start dmesg after boot without login (as a tty2 shell)
* Start htop after boot without login (as a tty3 shell)
* Upload reHDD log every 12h if wanted

### Software requirements
* `apt-get install hwinfo smartmontools curl htop sudo`

### Installation

clone this repo into /root/

```
git submodule init
git submodule update
```

`cd /root/reHDD/`

`make release`

`bash scripts/install_reHDD.bash`

If you want to upload the logs, edit `scripts/reHDDLogUploader.bash` with your nextcloud token

Add your system drive in `/root/reHDD/ignoreDrives.conf` like:
```e102f49d```
Get the first 8 Bytes from your UUID via `blkid /dev/sdX`

`reboot`

## Build docs 
`make docs`

open `doc/html/index.html` in browser
