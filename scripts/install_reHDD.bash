#!/bin/bash

cd /root/reHDD/scripts/

chmod +x reHDDLogUploader.bash
cp reHDDLogUploader.service /lib/systemd/system/reHDDLogUploader.service
cp reHDDLogUploader.timer /lib/systemd/system/reHDDLogUploader.timer
systemctl daemon-reload
systemctl enable /lib/systemd/system/reHDDLogUploader.timer

chmod +x reHDDStartHelper.bash
cp reHDDStartHelper.service /lib/systemd/system/reHDDStartHelper.service
systemctl daemon-reload
systemctl enable /lib/systemd/system/reHDDStartHelper.service

mkdir -p /lib/systemd/system/getty@tty1.service.d
cp getty@tty1.service.d_override.conf /lib/systemd/system/getty@tty1.service.d/override.conf
systemctl daemon-reload

mkdir -p /lib/systemd/system/getty@tty2.service.d
cp getty@tty2.service.d_override.conf /lib/systemd/system/getty@tty2.service.d/override.conf
systemctl daemon-reload

mkdir -p /lib/systemd/system/getty@tty3.service.d
cp getty@tty3.service.d_override.conf /lib/systemd/system/getty@tty3.service.d/override.conf
systemctl daemon-reload

