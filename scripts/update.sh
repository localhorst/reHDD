#! /usr/bin/bash

echo starting update

systemctl stop /lib/systemd/system/getty@tty1.service.d

cd /root/reHDD/

FILE=../ignoreDrives.conf
if test -f "$FILE"; then
   echo "backup exits already"
else
    cp /root/reHDD/ignoreDrives.conf /root/ignoreDrives.conf
fi

git reset

git stash force 

git stash

git checkout master

git pull

git submodule init

git submodule update

make clean

make release

cp  /root/ignoreDrives.conf /root/reHDD/ignoreDrives.conf

systemctl start /lib/systemd/system/getty@tty1.service.d